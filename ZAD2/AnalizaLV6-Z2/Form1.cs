﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalizaLV6_Z2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private char[] charArray;
        private void bStart_Click_1(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Boras\Desktop\AnalizaLV6-Z2\words.txt");
            Words word = new Words();
            Words.getRandom();
            foreach(string line in lines)
            {
                word.words.Add(line);
                Console.WriteLine("\t" + line);
                
            }
            int length = word.words[Words.IRnd].Length;
            string wordToGuess = word.words[Words.IRnd];
            lbChars.Text = "";
            int chances = 7;
            lbWord.Text = chances.ToString();
             lbCorrectLetters.Text = "";
            lbIncorrectLetters.Text = "";

            for (int i = 0; i < length; i++)
            {
                lbChars.Text = lbChars.Text + "_ ";
            }
            int characterLength = lbChars.Text.Length;
        }

        private void bSubmit_Click_1(object sender, EventArgs e)
        {
            int chances = Int32.Parse(lbWord.Text);

            int flag = 0;
            int flag1 = 0;
            int length = 0;
            
            string wordToGuess = "";
            if (flag == 0)
            {
                Words word = new Words();
                length = lbChars.Text.Length;
                length = length - length / 2;
                string[] lines = System.IO.File.ReadAllLines(@"C:\Users\zvoni\Desktop\AnalizaOOP\AnalizaLV6-Z2\words.txt");
                foreach (string line in lines)
                {
                    word.words.Add(line);
                    Console.WriteLine("\t" + line);

                }
                for (int i = 0; i < 5; i++)
                {
                    if (length == word.words[i].Length)
                    {
                        wordToGuess = word.words[i];
                    }
                }
                flag = 1;
            }

            string guessedWord = tbGuess.Text;
            string underscore = "";
            string name = "";
            if (flag1 == 0)
            {
                for (int i = 0; i < wordToGuess.Length; i++)
                {
                    underscore = underscore + "_ ";
                }

                charArray = underscore.ToCharArray();
                flag1 = 1;
            }
            if (wordToGuess == guessedWord)
            {
                lbChars.Text = wordToGuess;
                MessageBox.Show("Congratz, you won!");
            }
            else if (wordToGuess.Contains(guessedWord))
            {

                char[] word = wordToGuess.ToCharArray();
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == guessedWord[0])
                    {

                        if (i != 0)
                        {
                            charArray[2 * i] = guessedWord[0];
                        }
                        else
                        {
                            charArray[i] = guessedWord[0];
                        }
                    }
                }
                for (int i = 0; i < charArray.Length; i++)
                {
                    name = name + charArray[i].ToString();
                }

                charArray = name.ToCharArray();
                lbChars.Text = name;
                lbCorrectLetters.Text = lbCorrectLetters.Text + " " + guessedWord;



            }
            else
            {
                chances -= 1;
                lbWord.Text = chances.ToString();
                lbIncorrectLetters.Text = lbIncorrectLetters.Text + " " + guessedWord;
                if (chances == 0)
                {
                    MessageBox.Show("Better luck next time!");
                    Application.Exit();
                }
            }





        }
    }
}
